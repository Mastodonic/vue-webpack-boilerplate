
/*
 |--------------------------------------------------------------------------
 | Browser-sync config file
 |--------------------------------------------------------------------------
 |
 | For up-to-date information about the options:
 |   http://www.browsersync.io/docs/options/
 |
 | There are more options than you see here, these are just the ones that are
 | set internally. See the website for more info.
 |
 |
 */

//Reading .env file
var fs = require('fs');
var env = fs.readFileSync('.env', 'utf-8');

var vhost = function() {
    var str = env.match(/FE_VHOST="([^\s]+)"/);
    if (str) {
        return str[1];
    } else {
        throw 'Can not find FE_VHOST value in .env file';
    }
};

module.exports = {
    'ui': {
        'port': 3001,
        'weinre': {
            'port': 8080
        }
    },
    'files': [
        '**/style.min.css',
        '**/app.min.js',
        '**/wp-content/themes/**/*.php',
        '**/wp-content/themes/**/*.jpg',
        '**/wp-content/themes/**/*.png',
        '**/wp-content/themes/**/*.svg'
    ],
    'watchOptions': {},
    'server': false,
    'proxy': {
        target: vhost()
    },
    'port': 3000,
    'middleware': false,
    'serveStatic': [],
    'ghostMode': {
        'clicks': true,
        'scroll': true,
        'forms': {
            'submit': true,
            'inputs': true,
            'toggles': true
        }
    }
};
